#
# Adrián Chaves (Gallaecio) <adrian@chaves.io>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: websites-planet-kde-org 1.0\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-05 12:42+0000\n"
"PO-Revision-Date: 2023-04-28 20:06+0200\n"
"Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>\n"
"Language-Team: Galician <proxecto@trasno.gal>\n"
"Language: gl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.04.0\n"

#: config.yaml:0
msgid "Planet KDE"
msgstr "Planeta KDE"

#: config.yaml:0
msgid "Planet KDE site providing newest news from the KDE Project"
msgstr "Planeta KDE, o sitio de novas do proxecto KDE"

#: config.yaml:0
msgid "Add your own feed"
msgstr "Engada a súa propia fonte"

#: i18n/en.yaml:0
msgid "Welcome to Planet KDE"
msgstr "Benvida ao Planeta KDE"

#: i18n/en.yaml:0
msgid ""
"This is a feed aggregator that collects what the contributors to the [KDE "
"community](https://kde.org) are writing on their respective blogs, in "
"different languages"
msgstr ""
"Este é un agregador de novas que recolle o que as persoas que colaboran na "
"[comunidade KDE](https://kde.org) escriben nos seus blogs, en distintos "
"idiomas"

#: i18n/en.yaml:0
msgid "Go down one post"
msgstr "Seguinte publicación"

#: i18n/en.yaml:0
msgid "Go up one post"
msgstr "Publicación anterior"
