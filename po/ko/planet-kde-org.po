#
# Shinjo Park <kde@peremen.name>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: websites-planet-kde-org 1.0\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-05 12:42+0000\n"
"PO-Revision-Date: 2021-05-20 02:02+0200\n"
"Last-Translator: Shinjo Park <kde@peremen.name>\n"
"Language-Team: Korean <kde-kr@kde.org>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Lokalize 20.12.3\n"

#: config.yaml:0
msgid "Planet KDE"
msgstr "플래닛 KDE"

#: config.yaml:0
msgid "Planet KDE site providing newest news from the KDE Project"
msgstr "플래닛 KDE 사이트는 KDE 프로젝트의 최신 뉴스를 제공합니다"

#: config.yaml:0
msgid "Add your own feed"
msgstr "내 피드 추가하기"

#: i18n/en.yaml:0
msgid "Welcome to Planet KDE"
msgstr "플래닛 KDE에 오신 것을 환영합니다"

#: i18n/en.yaml:0
msgid ""
"This is a feed aggregator that collects what the contributors to the [KDE "
"community](https://kde.org) are writing on their respective blogs, in "
"different languages"
msgstr ""
"이 목록은 [KDE 커뮤니티](https://kde.org) 구성원이 다양한 언어로 개별 블로그"
"에 쓰는 글을 모아서 보여 줍니다"

#: i18n/en.yaml:0
msgid "Go down one post"
msgstr "한 글 아래로 이동"

#: i18n/en.yaml:0
msgid "Go up one post"
msgstr "한 글 위로 이동"
